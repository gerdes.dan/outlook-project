# Data Analysis of Outlook on Life Surveys, 2012
### Data gathered by:
	- Belinda Robnett (University of California-Irvine)
	- Katherine Tate (Brown University)
### Provided by:
	- Inter-university Consortium for Political and Social Research (#35348)

#### 2012 web survey gathering observations on the societal well being of Americans. Some variables within scope are politics, religion, gender relation, race relation, family relations, as well as others.

This project seeks to analyze the data and find correlations amongst the data to describe the American feelings and beliefs across the spectrum in 2012.

##### This is my first data analysis project as I am attempting to learn data science. I have very limited understanging of both the science and the coding. Any tips will always be appreciated. I have also included a time stamped progress blog to explain my thinking and reasoning. 
