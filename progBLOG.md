# 2012 Outlook Survey Progress Blog

**2018-10-06 19:20:00**

- Today I am starting my very first data analysis project, guided by a course on coursera. I was required to choose 1 data set out of 5 that I want to study, and then develop a question that I want to answer by analyzing the data.
- The observed data comes via an online survey from 2012, where the respondents were either male or females or other, ages >=18. The respondents were also classified as either black or white/other. The variables are questions asked in the survey in regards to their personal position on race, religion, politics, gender/sex, as well as others.
- I have only added this blog, an ipython file, and the data set to the public gitlab repository so far. I _have not_ yet formulated a question to study and a problem to be solved.

**2018-10-06 20:08:00**

- I have now developed a question that I would like to study the data for an answer to:
	"Did a person's race correlate with who they voted for in 2012?"

**2018-10-06 21:15:00**

- Having reviewed the rest of the section on coursera, I realized I was missing quite a lot in my initial diagnosis of how I needed to use the scientific method to put together a good research proposal.
- With my first question relating to whether or not race had a noticeable correlation in a person's vote, I picked out variables that I believe would point to answers. I picked
race(PPETHM), did they vote(W1\_A4), if so who for(W1\_A5A, W1\_A5A\_OTHER\_TEXT)?, if not who would they have chosen(W1\_A5B,W1\_A5B\_OTHER\_TEXT)? Here I hypothosize that minorities voted for Barrack Obama in greater force than they did for McCain, as he was poised to be the first person from a minority to be elected as president of the United States.
- I also developed a similar second question, however, instead I asked whether or not the person's vote correlated with their gender. Similarly, I used all of the same variables except instead of using race, I used (PPGENDER) in order to point to votes by gender. I hypothesize that females may have voted in greater numbers for Barrack Obama, as women tend to be a bit more liberal then men. Though, that is a mere anicdotal belief. I also think that there was a belief that he would protect 'womens rights' more than his opponent.

**2018-10-08 02:18:00** 

I spent quite a bit of time today trying to organize the data in a way that was easy to understand. I formulated each ethnicity/gender pair into rows with how they voted in the presidential election. I didnt include non-answers or non-voters, but I can do that very easy as that could be something important to look at as well.

I also spent a decent ammount of time trying to create data visualization. It was a lot harder than I thought it would be. I wound up putting each row into a subplot that was a horizontal bar chart. I still have a lot of work to make it look nice. I am a bit ahead of where I should be in the class, but that's okay i'll spend more time learning the basics.
